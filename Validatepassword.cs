﻿// mobile number should be 10 digits
//should include country code,91
// it can constain white space or hyphon after 91
//+sign may be present or not
//the first digits of mob numbes 6-9
//the rest of the digits can be b/t 0-9


using System.Text.RegularExpressions;

namespace MobileNumberValid
{
    internal class Validatepassword
    {

        //At least one upper case
        //at least one lower case
        //atleast one special charecter
        //atleast one number
        //minimum 8 charecter length
        public const string pattern = @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@#$%*&?])[A-Za-z\d@$!#%&]{6,20}$";
        public static bool ValidatePassword(string password)
        {
            if (password != null)
                return Regex.IsMatch(password, pattern);

            else return false;
        }
    }
}
